package net.resiststan.customtelegrambot;

import net.resiststan.customtelegrambot.util.IUpdateHandler;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.telegram.abilitybots.api.bot.AbilityBot;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.bots.DefaultBotOptions;
import org.telegram.telegrambots.meta.ApiContext;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.io.Serializable;

import static net.resiststan.filmlist.util.ParameterLoader.getParam;

public class CustomBot {
    private final IUpdateHandler processingUpdate;

    public CustomBot(IUpdateHandler processingUpdate) {
        this.processingUpdate = processingUpdate;
    }

    public void start() {
        registrationBot();
    }

    private void registrationBot() {
        System.out.println("### Start bot initializer... ###");

        try {
            prepareBotInit();

            //инициализация api
            ApiContextInitializer.init();
            TelegramBotsApi telegramBotsApi = new TelegramBotsApi();
            DefaultBotOptions botOptions = ApiContext.getInstance(DefaultBotOptions.class);

            //настраиваем прокси
            if (isUseProxy()) {
                setProxy(botOptions);
            }

            //регистрация бота
            System.out.println("### Bot registration... ###");
            telegramBotsApi.registerBot(new Bot(
                    getParam("TB_BOT_TOKEN"),
                    getParam("TB_BOT_NAME"),
                    botOptions
            ));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void prepareBotInit() throws Exception {
        validateBotParam("TB_BOT_TOKEN");
        validateBotParam("TB_BOT_NAME");
    }

    private void validateBotParam(String param) throws Exception {
        if (StringUtils.isEmpty(getParam(param))) {
            System.out.println("### Cancel bot initializer ###");
            throw new Exception(param + " is undefined");
        }
    }

    private boolean isUseProxy() {
        return StringUtils.equals(getParam("TB_USE_PROXY"), "true");
    }

    private void setProxy(DefaultBotOptions botOptions) {
        System.out.println("### Set proxy... ###");
        System.out.println(getParam("TB_PROXY_HOST"));
        System.out.println(getParam("TB_PROXY_PORT"));

        //если запуск на локальной машине - используем прокси
        HttpHost httpHost = new HttpHost(
                getParam("TB_PROXY_HOST"),
                Integer.parseInt(getParam("TB_PROXY_PORT"))
        );

        RequestConfig requestConfig = RequestConfig
                .custom()
                .setProxy(httpHost)
                .setAuthenticationEnabled(false)
                .build();

        botOptions.setRequestConfig(requestConfig);
    }

    private CredentialsProvider createCredentials() {
        CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
        credentialsProvider.setCredentials(
                new AuthScope(getParam("TB_PROXY_HOST"), Integer.parseInt(getParam("TB_PROXY_PORT"))),
                new UsernamePasswordCredentials(getParam("TB_PROXY_USER_NAME"), getParam("TB_PROXY_PASSWORD"))
        );

        return credentialsProvider;
    }

    public static boolean hasProxyAuthentication() {
        return StringUtils.isNotEmpty(getParam("TB_PROXY_LOGIN"))
                && StringUtils.isNotEmpty(getParam("TB_PROXY_PASSWORD"));
    }

    private class Bot extends AbilityBot {

        Bot(String botToken, String botUsername, DefaultBotOptions options) {
            super(botToken, botUsername, options);
        }

        @Override
        public int creatorId() {
            return Integer.parseInt(getParam("TB_CREATOR_ID"));
        }

        @Override
        public void onUpdateReceived(Update update) {
            try {
                BotApiMethod<? extends Serializable> method = processingUpdate.handle(update);

                if (method != null) {
                    execute(method);
                }

            } catch (TelegramApiException e) {
                e.printStackTrace();
            }
        }

    }
}
