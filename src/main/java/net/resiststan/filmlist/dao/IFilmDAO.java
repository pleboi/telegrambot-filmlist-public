package net.resiststan.filmlist.dao;

import java.io.Serializable;

public interface IFilmDAO<T, ID extends Serializable> {
    T add(T t);

    void remove(ID id);

    T markAsWatch(ID id);

    T getUserRandomFilm(Integer userId);

    Page<T> paginationUserFilm(Integer userId, Integer pageNumber, Integer pageSize);
}
