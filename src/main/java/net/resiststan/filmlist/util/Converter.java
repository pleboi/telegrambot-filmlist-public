package net.resiststan.filmlist.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.BasicDBObject;
import org.bson.Document;

import java.io.IOException;

public class Converter {
    public static Object toPojo(Document document, Class<?> valueType) throws IOException {
        if (document != null) {
            return new ObjectMapper()
                    .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                    .readValue(document.toJson(), valueType);
        }

        return null;
    }

    public static Document toDocument(Object obj, Class<?> valueType) throws JsonProcessingException {
        return Document.parse(new ObjectMapper().writer().withDefaultPrettyPrinter().writeValueAsString(valueType.cast(obj)));
    }

    public static BasicDBObject toBasicDBObject(Object obj, Class<?> valueType) throws JsonProcessingException {
        return new BasicDBObject(toDocument(obj, valueType));
    }

    public static BasicDBObject toBasicDBObject(String fieldName, Object obj, Class<?> valueType) throws JsonProcessingException {
        return new BasicDBObject(fieldName, toDocument(obj, valueType));
    }
}
