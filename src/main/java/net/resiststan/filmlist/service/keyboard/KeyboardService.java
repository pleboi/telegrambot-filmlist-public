package net.resiststan.filmlist.service.keyboard;

import net.resiststan.customtelegrambot.util.keyboard.InlineKeyboardBuilder;
import net.resiststan.customtelegrambot.util.keyboard.ReplyKeyboardBuilder;
import net.resiststan.filmlist.dao.Page;
import net.resiststan.filmlist.entity.Film;
import net.resiststan.filmlist.entity.SimpleFilmInfo;
import net.resiststan.filmlist.util.DictionaryShelf;
import org.javatuples.Pair;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboard;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;

import java.util.List;

public class KeyboardService implements IKeyboardService {

    private ReplyKeyboardMarkup defaultKeyboard(DictionaryShelf.Dictionary dictionary) {
        return ReplyKeyboardBuilder.create()
                .row(dictionary.get("btn.txt.random.film"))
                .row(dictionary.get("btn.txt.all.film"))
                .row(dictionary.get("btn.txt.help"), dictionary.get("btn.txt.info"))
//                .row(dictionary.get("btn.txt.all.film")/*, dictionary.get("btn.txt.all.nw.film")*/)
                .build();
    }

    private InlineKeyboardMarkup randomFilmIK(DictionaryShelf.Dictionary dictionary, Film film) {
        return InlineKeyboardBuilder.create()
                .row()
                .button(dictionary.get("btn.txt.mark.watch"), createCallbackData("cb.watched.film", film.getId()))
                .button(dictionary.get("btn.txt.del.film"), createCallbackData("cb.remove.film", film.getId()))
                .endRow()
                .row()
                .button(dictionary.get("btn.txt.next"), createCallbackData("cb.another.random.film"))
                .endRow()
                .build();
    }

    private static InlineKeyboardMarkup aftMarkAsWatchIK(DictionaryShelf.Dictionary dictionary, Film film) {
        return InlineKeyboardBuilder.create()
                .row()
                .button(dictionary.get("btn.txt.del.film"), createCallbackData("cb.remove.film", film.getId()))
                .endRow()
                .row()
                .button(dictionary.get("btn.txt.next"), createCallbackData("cb.another.random.film"))
                .endRow()
                .build();
    }

    private static InlineKeyboardMarkup aftRemoveIK(DictionaryShelf.Dictionary dictionary) {
        return InlineKeyboardBuilder.create()
                .row()
                .button(dictionary.get("btn.txt.next"), createCallbackData("cb.another.random.film"))
                .endRow()
                .build();
    }

    private static InlineKeyboardMarkup filmSearchResultIK(DictionaryShelf.Dictionary dictionary, Pair<Integer, List<SimpleFilmInfo>> data) {
        Integer position = data.getValue0();
        List<SimpleFilmInfo> filmList = data.getValue1();


        int prevPosition = position - 1;
        int nextPosition = position + 1;

        boolean isShowPrevBtn = prevPosition > 0;
        boolean isShowNextBtn = nextPosition < filmList.size();

        return InlineKeyboardBuilder.create()
                .row()
                    .button(!isShowPrevBtn, "*", createCallbackData("cb.do.nothing"))
                    .button(isShowPrevBtn, dictionary.get("btn.txt.prev.film"), createCallbackData("ca.other.film", prevPosition))
                    .button(!isShowNextBtn, "*", createCallbackData("cb.do.nothing"))
                    .button(isShowNextBtn, dictionary.get("btn.txt.next.film"), createCallbackData("ca.other.film", nextPosition))
                .endRow()
                .row()
                    .button(dictionary.get("btn.txt.add.film"), createCallbackData("ca.add.current.film", filmList.get(position).getFilmId()))
                .endRow()
                .build();
    }

    private static ReplyKeyboard filmListIK(DictionaryShelf.Dictionary dictionary, Page<Film> page) {
        int currPage = page.getPageNumber();
        long pageTotalCount = page.getPageTotalCount();

        boolean prevPageCondition = currPage > 1;
        boolean nextPageCondition = pageTotalCount != currPage;

        int prevPage = currPage - 1;
        int nextPage = currPage + 1;

        String currPageBtnTxt = String.format(dictionary.get("msg.format.page.of"), currPage, pageTotalCount);

        return InlineKeyboardBuilder.create()
                .row()
                    .button(prevPageCondition, dictionary.get("btn.txt.first.page"), createCallbackData("cb.go.to.page", 1))
                    .button(prevPageCondition, dictionary.get("btn.txt.prev.page"), createCallbackData("cb.go.to.page", prevPage))
                    .button(currPageBtnTxt, createCallbackData("cb.do.nothing"))
                    .button(nextPageCondition, dictionary.get("btn.txt.next.page"), createCallbackData("cb.go.to.page", nextPage))
                    .button(nextPageCondition, dictionary.get("btn.txt.last.page"), createCallbackData("cb.go.to.page", pageTotalCount))
                .endRow()
                .build();
    }

    private static String createCallbackData(String key, Object value) {
        return DictionaryShelf.get(key) + DictionaryShelf.get("callback.data.delimiter") + value;
    }

    private static String createCallbackData(String key) {
        return DictionaryShelf.get(key);
    }

    @Override
    public ReplyKeyboard getKeyboard(IKeyboardType type, DictionaryShelf.Dictionary dictionary, Object param) {
        if (Reply.DEFAULT.equals(type)) {
            return defaultKeyboard(dictionary);

        } else if (Inline.RANDOMFILM.equals(type)) {
            return randomFilmIK(dictionary, (Film) param);

        } else if (Inline.WATCH.equals(type)) {
            return aftMarkAsWatchIK(dictionary, (Film) param);

        } else if (Inline.REMOVE.equals(type)) {
            return aftRemoveIK(dictionary);

        } else if (Inline.FILMLIST.equals(type)) {
            return filmListIK(dictionary, (Page<Film>) param);

        } else if (Inline.SEARCHFILM.equals(type)) {
            return filmSearchResultIK(dictionary, (Pair<Integer, List<SimpleFilmInfo>>) param);
        }

        return null;
    }

    public enum Reply implements IKeyboardType {
        DEFAULT
    }

    public enum Inline implements IKeyboardType {
        RANDOMFILM,
        WATCH,
        REMOVE,
        FILMLIST,
        SEARCHFILM
    }
}
