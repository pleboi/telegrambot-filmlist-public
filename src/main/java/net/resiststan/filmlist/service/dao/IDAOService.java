package net.resiststan.filmlist.service.dao;

import net.resiststan.filmlist.dao.Page;
import net.resiststan.filmlist.entity.Film;
import net.resiststan.filmlist.service.AbsServiceKeeper;

public interface IDAOService extends AbsServiceKeeper.IService {
    Film getUserRandomFilm(Integer id);

    void add(Film film);

    Film markAsWatchFilm(String filmId);

    void removeFilm(String filmId);

    Page<Film> getUserFilmList(Integer userId, Integer pageNumber);
}
