package net.resiststan.filmlist.service.dao;

import net.resiststan.filmlist.dao.FilmMongoDB;
import net.resiststan.filmlist.dao.IFilmDAO;
import net.resiststan.filmlist.dao.Page;
import net.resiststan.filmlist.entity.Film;

public class DAOService implements IDAOService {
    private final IFilmDAO<Film, String> filmDAO;

    public DAOService() {
        filmDAO = new FilmMongoDB();
    }

    @Override
    public Film getUserRandomFilm(Integer userId) {
        return filmDAO.getUserRandomFilm(userId);
    }

    @Override
    public void add(Film film) {
        filmDAO.add(film);
    }

    @Override
    public Film markAsWatchFilm(String filmId) {
        return filmDAO.markAsWatch(filmId);
    }

    @Override
    public void removeFilm(String filmId) {
        filmDAO.remove(filmId);
    }

    @Override
    public Page<Film> getUserFilmList(Integer userId, Integer pageNumber) {
        final int pageSize = 5;
        return filmDAO.paginationUserFilm(userId, pageNumber, pageSize);
    }
}
