package net.resiststan.filmlist.service.datagenerator;

import net.resiststan.filmlist.entity.Film;
import net.resiststan.filmlist.entity.SimpleFilmInfo;
import net.resiststan.filmlist.service.datagenerator.exceptions.StorageAccessException;
import net.resiststan.filmlist.service.datagenerator.fimlstorage.kp.IKinopoisk;
import net.resiststan.filmlist.service.datagenerator.fimlstorage.kp.KinopoiskFilmList;
import net.resiststan.filmlist.service.datagenerator.fimlstorage.kp.KinopoiskFilmSingle;
import net.resiststan.filmlist.util.Common;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import static net.resiststan.filmlist.service.datagenerator.DataStorage.IDataStorage;

public class DataGeneratorService implements IDataGeneratorService {
    @Override
    public Film getFilm(String text) throws IOException, StorageAccessException {
        if (Common.isUrl(text)) {
            return handleLikeUrl(text);
        } else {
            return handleLikeText(text);
        }
    }

    @Override
    public List<SimpleFilmInfo> getFilmList(String text) throws IOException, StorageAccessException {
//        return (List<SimpleFilmInfo>) handle(KinopoiskFilmList.class, text);
        return new KinopoiskFilmList(text).get();
    }

    private Film handleLikeUrl(String text) throws IOException, StorageAccessException {
        if (text.indexOf(IKinopoisk.URL) == 0) {
            return (Film) handle(KinopoiskFilmSingle.class, text);
        }

        return null;
    }

    private Film handleLikeText(String text) throws StorageAccessException {
        try {
            //тут надо подумать...
            //может быть что то кроме кп, так же может приниматься не ссылка а какой то запрос
            //пока ничего другого нет - будет так...
            String url = IKinopoisk.FILM_URL + text;

            return handleLikeUrl(url);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private Object handle(Class<? extends IDataStorage> clazz, String text) throws IOException, StorageAccessException {
        try {
            return getConstructor(clazz).newInstance(text).get();

        } catch (IllegalAccessException | InvocationTargetException | InstantiationException e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }

    private Constructor<? extends IDataStorage> getConstructor(Class<? extends IDataStorage> clazz) throws IOException, StorageAccessException {
        try {
            return clazz.getConstructor(String.class);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }
}
