package net.resiststan.filmlist.service.datagenerator.fimlstorage.kp;

import net.resiststan.filmlist.entity.Film;
import net.resiststan.filmlist.service.datagenerator.exceptions.StorageAccessException;
import net.resiststan.filmlist.service.datagenerator.fimlstorage.AbsParser;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class KinopoiskFilmSingle extends AbsParser<Film> implements IKinopoisk {
    private Element infoTableElem;
    private final Film film;

    public KinopoiskFilmSingle(String url) throws IOException, StorageAccessException {
        super(url);

        infoTableElem = rootDocument.getElementById("infoTable");

        film = new Film();

        film.setUserId(null);
        film.setStorageFilmId(getFilmId());
        film.setName(getName());
        film.setNameOriginal(getNameOriginal());
        film.setYear(getYear());
        film.setCountry(getCountry());
        film.setTime(getTime());
        film.setGenre(getGenre());
        film.setWatch(false);
        film.setWatchDate(null);
        film.setDescription(getDescription());
        film.setUrl(url);
    }

    @Override
    public Film get() {
        return film;
    }

    @Override
    public boolean invalidDocument() {
        return false;
    }

    private Integer getFilmId() {
        try {
            String filmId = rootDocument.select("#movie-trailer-block").first().attr("data-film-id");

            return Integer.valueOf(filmId);
        } catch (NumberFormatException | NullPointerException e) {
            System.err.println(Arrays.toString(e.getStackTrace()));
        }

        return null;
    }

    private String getNameOriginal() {
        try {
            return rootDocument.select(".alternativeHeadline").text();

        } catch (NumberFormatException | NullPointerException e) {
            System.err.println(Arrays.toString(e.getStackTrace()));
        }

        return null;
    }

    private Integer getYear() {
        try {
            return Integer.valueOf(infoTableElem.select("tbody > tr:eq(0) td:eq(1) > div > a").text());

        } catch (NumberFormatException | NullPointerException e) {
            System.err.println(Arrays.toString(e.getStackTrace()));
        }

        return null;
    }

    private String getName() {
        try {
            return rootDocument.select(".moviename-title-wrapper").text();

        } catch (NumberFormatException | NullPointerException e) {
            System.err.println(Arrays.toString(e.getStackTrace()));
        }

        return null;
    }

    private List<String> getCountry() {
        try {
            List<String> countryList = new ArrayList<>();

            Elements element = infoTableElem.select("tbody > tr:eq(1) > td:eq(1) > div > a[href]");

            for (Element elem : element) {
                countryList.add(elem.text());
            }

            return countryList;

        } catch (NumberFormatException | NullPointerException e) {
            System.err.println(Arrays.toString(e.getStackTrace()));
        }

        return null;
    }

    private Integer getTime() {
        try {
            return Integer.valueOf(rootDocument.getElementById("runtime").text().split(" ")[0]);

        } catch (NumberFormatException | NullPointerException e) {
            System.err.println(Arrays.toString(e.getStackTrace()));
        }

        return null;
    }

    private List<String> getGenre() {
        try {
            Elements element = infoTableElem.select("tbody > tr:eq(10) > td:eq(1) > span");
            Elements hrefs = element.select("a[href]");

            List<String> genreList = new ArrayList<>();

            for (Element elem : hrefs) {
                genreList.add(elem.text());
            }


            return genreList;

        } catch (NumberFormatException | NullPointerException e) {
            System.err.println(Arrays.toString(e.getStackTrace()));
        }

        return null;
    }

    private String getDescription() {
        try {
            return rootDocument.getElementById("block_left_padtop").select("div.brand_words.film-synopsys").text();

        } catch (NumberFormatException | NullPointerException e) {
            System.err.println(Arrays.toString(e.getStackTrace()));
        }

        return null;
    }
}
