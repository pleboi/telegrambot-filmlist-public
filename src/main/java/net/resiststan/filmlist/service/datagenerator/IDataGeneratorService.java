package net.resiststan.filmlist.service.datagenerator;

import net.resiststan.filmlist.entity.Film;
import net.resiststan.filmlist.entity.SimpleFilmInfo;
import net.resiststan.filmlist.service.datagenerator.exceptions.StorageAccessException;
import net.resiststan.filmlist.service.AbsServiceKeeper;

import java.io.IOException;
import java.util.List;

public interface IDataGeneratorService extends AbsServiceKeeper.IService {
    Film getFilm(String text) throws IOException, StorageAccessException;

    List<SimpleFilmInfo> getFilmList(String text) throws IOException, StorageAccessException;
}
