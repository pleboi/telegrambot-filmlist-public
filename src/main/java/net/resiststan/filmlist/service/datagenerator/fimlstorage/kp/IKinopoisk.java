package net.resiststan.filmlist.service.datagenerator.fimlstorage.kp;

public interface IKinopoisk {
    String URL = "https://www.kinopoisk.ru";
    String FILM_URL = "https://www.kinopoisk.ru/film/";
}
