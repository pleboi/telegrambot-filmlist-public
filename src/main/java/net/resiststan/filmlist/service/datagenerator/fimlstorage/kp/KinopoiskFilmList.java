package net.resiststan.filmlist.service.datagenerator.fimlstorage.kp;

import net.resiststan.filmlist.entity.SimpleFilmInfo;
import net.resiststan.filmlist.service.datagenerator.exceptions.StorageAccessException;
import net.resiststan.filmlist.service.datagenerator.fimlstorage.AbsElementParser;
import net.resiststan.filmlist.service.datagenerator.fimlstorage.AbsParser;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class KinopoiskFilmList extends AbsParser<List<SimpleFilmInfo>> implements IKinopoisk {
    private List<SimpleFilmInfo> simpleFilmInfoList;

    public KinopoiskFilmList(String queryStr) throws IOException, StorageAccessException {
        super(createUrl(queryStr));

        simpleFilmInfoList = new ArrayList<>();
        rootDocument
                .selectFirst("#block_left_pad div.search_results:has(span.num)").select("div.info")
                .iterator()
                .forEachRemaining(x -> {
                    if (isFilmDataType(x)) {
                        SimpleFilmInfo film = new ElementFilmParser(x).get();
                        film.setQueryStr(queryStr);

                        simpleFilmInfoList.add(film);
                    }
                });
    }

    @Override
    public List<SimpleFilmInfo> get() {
        return simpleFilmInfoList;
    }

    @Override
    public boolean invalidDocument() {
        return false;
    }


    private boolean isFilmDataType(Element element) {
        return element.select("p.name > a").attr("data-type").equals("film");
    }

    private static String createUrl(String queryStr) {
        String urlPref = URL + "/s/type/film/list/1/find/";
        String urlPost = "/order/relevant/perpage/25/";

        return urlPref + queryStr + urlPost;
    }

    private static class ElementFilmParser extends AbsElementParser<SimpleFilmInfo> {
        private final SimpleFilmInfo film;

        ElementFilmParser(Element rootElement) {
            super(rootElement);

            film = new SimpleFilmInfo();

            film.setFilmId(getFilmId());
            film.setCountry(getCountry());
            film.setName(getName());
            film.setOriginalName(getOriginalName());
            film.setTime(getTime());
            film.setYear(getYear());
            film.setDirector(getDirector());
            film.setGenre(getGenre());
            film.setUrl(getUrl());
        }

        @Override
        public SimpleFilmInfo get() {
            return film;
        }

        private String getFilmId() {
            try {
                return rootElement.select("p.name > a").attr("data-id");

            } catch (NullPointerException e) {
                System.err.println(Arrays.toString(e.getStackTrace()));
            }

            return null;
        }

        private String getName() {
            try {
                return rootElement.select("p.name > a").text();

            } catch (NumberFormatException | NullPointerException e) {
                System.err.println(Arrays.toString(e.getStackTrace()));
            }

            return null;
        }

        private String getYear() {
            try {
                return rootElement.select("p.name > span.year").text();

            } catch (NumberFormatException | NullPointerException e) {
                System.err.println(Arrays.toString(e.getStackTrace()));
            }

            return null;
        }

        private String getGenre() {
            try {
                return rootElement
                        .select("span.gray:has(i.director) > br").first()
                        .nextSibling()
                        .toString().replaceAll(".*\\(|\\).*", "");

            } catch (NullPointerException e) {
                System.err.println(e.getMessage());
                return null;
            }
        }

        private String getCountry() {
            try {
                return rootElement
                        .select("span.gray:has(i.director) > i").first()
                        .previousSibling()
                        .toString().replaceAll(",.*", "");

            } catch (NullPointerException e) {
                return null;
            }
        }

        private String getDirector() {
            return rootElement.select("i.director > a").text();
        }

        private String getTime() {
            String[] arr = getOriginalNameAndTime(rootElement);

            if (arr != null) {
                if (arr.length > 1) {
                    return getOnlyDigits(arr[1]);
                } else {
                    if (arr[0].contains("мин")) {
                        return getOnlyDigits(arr[0]);
                    } else {
                        return "";
                    }
                }
            }

            return null;
        }

        private String getOriginalName() {
            String[] arr = getOriginalNameAndTime(rootElement);

            if (arr != null) {
                if (arr.length > 1) {
                    return arr[0];
                } else {
                    if (arr[0].contains("мин")) {
                        return "";
                    } else {
                        return arr[0];
                    }
                }
            }

            return null;
        }

        private String getOnlyDigits(String str) {
            return str.replaceAll("\\D+", "");
        }

        private String[] getOriginalNameAndTime(Element element) {
            try {
                return element.select("div.info > span.gray").first().text().split(",");

            } catch (NumberFormatException | NullPointerException e) {
                System.err.println(Arrays.toString(e.getStackTrace()));
            }

            return null;
        }

        private String getUrl() {
            String filmId = getFilmId();

            if (filmId != null) {
                return FILM_URL + getFilmId();
            } else {
                return getUrlData();
            }
        }

        private String getUrlData() {
            try {
                return URL + rootElement.select("p.name > a").attr("data-url");

            } catch (NullPointerException e) {
                System.err.println(Arrays.toString(e.getStackTrace()));
            }

            return null;
        }
    }
}
