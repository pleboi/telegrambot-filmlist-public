package net.resiststan.filmlist.service.datagenerator;


import org.jetbrains.annotations.NotNull;

public class DataStorage {
    public static IDataStorage setContext(@NotNull IDataStorage ds) {
        return ds;
    }

    public interface IDataStorage<T> {
        T get();
    }
}
