package net.resiststan.filmlist.service;

import java.util.HashMap;
import java.util.Map;

public abstract class AbsServiceKeeper {
    private final static Map<Class<? extends IService>, IService> SERVICE_MAP = new HashMap<>();

    private static <T extends IService> T addNewService(Class<T> tClass) {
        try {
            IService service = tClass.newInstance();
            SERVICE_MAP.put(tClass, service);

            return tClass.cast(service);

        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException();
        }
    }

    public static <T extends IService> T getService(Class<T> tClass) {
        if (SERVICE_MAP.containsKey(tClass)) {
            return tClass.cast(SERVICE_MAP.get(tClass));
        }

        return addNewService(tClass);
    }

    public interface IService { }
}
