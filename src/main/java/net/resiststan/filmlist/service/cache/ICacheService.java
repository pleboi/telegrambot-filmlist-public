package net.resiststan.filmlist.service.cache;

import net.resiststan.filmlist.service.AbsServiceKeeper;

public interface ICacheService<K, V> extends AbsServiceKeeper.IService {
    V get(K key);

    void put(K key, V value);

    boolean contain(K key);
}
