package net.resiststan.filmlist.service.messagedata;

import net.resiststan.filmlist.dao.Page;
import net.resiststan.filmlist.entity.Film;
import net.resiststan.filmlist.entity.SimpleFilmInfo;
import net.resiststan.filmlist.service.cache.CacheService.CacheKey;
import net.resiststan.filmlist.service.cache.ICacheService;
import net.resiststan.filmlist.service.datagenerator.exceptions.StorageAccessException;
import net.resiststan.filmlist.service.keyboard.KeyboardService;
import net.resiststan.filmlist.service.method.IMessageData;
import net.resiststan.filmlist.service.method.MessageDataImpl;
import net.resiststan.filmlist.util.DictionaryShelf;
import org.apache.commons.lang3.StringUtils;
import org.javatuples.Pair;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboard;

import java.io.IOException;
import java.util.List;
import java.util.Objects;

import static net.resiststan.filmlist.service.keyboard.KeyboardService.Inline;
import static net.resiststan.filmlist.service.keyboard.KeyboardService.Reply;
import static net.resiststan.filmlist.tbot.execute.AbsAnswerFactory.*;
import static net.resiststan.filmlist.util.DictionaryShelf.Dictionary;

public class MessageDataService implements IMessageDataService {

    @Override
    public IMessageData getSimpleMessage(Dictionary dictionary, String txt) {
        return new MessageDataImpl(
                dictionary.get(txt),
                getKeyboardService().getKeyboard(Reply.DEFAULT, dictionary, null)
        );
    }

    @Override
    public IMessageData getRandomFilm(Integer userId, Dictionary dictionary) {
        Film film = getDAOService().getUserRandomFilm(userId);

        String txt;
        ReplyKeyboard keyboard;
        if (Objects.nonNull(film)) {
            txt = String.format(
                    dictionary.get("msg.format.random.film"),
                    film.tittle(),
                    film.getYear(),
                    film.getTime());


            keyboard = getKeyboardService().getKeyboard(Inline.RANDOMFILM, dictionary, film);
        } else {
            txt = dictionary.get("msg.cant.find");
            keyboard = null;
        }

        return new MessageDataImpl(txt, keyboard);
    }

    @Override
    public IMessageData addFilm(Message message, Dictionary dictionary) {
        return addFilm(message.getText(), message.getFrom().getId(), dictionary);
    }

    @Override
    public IMessageData addFilm(CallbackQuery callbackQuery, Dictionary dictionary) {
        String text = getCallbackValue(callbackQuery);
        return addFilm(text, callbackQuery.getFrom().getId(), dictionary);
    }

    @Override
    public IMessageData markAsWatchFilm(CallbackQuery callbackQuery, Dictionary dictionary) {
        Film film = getDAOService().markAsWatchFilm(getCallbackValue(callbackQuery));

        String resultText;
        KeyboardService.Inline inlineType;

        if (Objects.nonNull(film)) {
            resultText = "msg.mark.as.watched";
            inlineType = KeyboardService.Inline.WATCH;
        } else {
            resultText = "msg.mb.film.not.exist";
            inlineType = KeyboardService.Inline.REMOVE;
        }

        return new MessageDataImpl(
                dictionary.get(resultText),
                IMessageData.TextPosition.APPEND,
                getKeyboardService().getKeyboard(inlineType, dictionary, film)
        );
    }

    @Override
    public IMessageData removeFilm(CallbackQuery callbackQuery, Dictionary dictionary) {
        getDAOService().removeFilm(getCallbackValue(callbackQuery));

        return new MessageDataImpl(
                dictionary.get("msg.remove.film"),
                IMessageData.TextPosition.APPEND,
                getKeyboardService().getKeyboard(Inline.REMOVE, dictionary, null)
        );
    }

    @Override
    public IMessageData getUserPageFilm(Message message, Dictionary dictionary) {
        int page = 1;
        return getPageData(message.getFrom().getId(), page, dictionary);
    }

    @Override
    public IMessageData getUserPageFilm(CallbackQuery callbackQuery, Dictionary dictionary) {
        int page = Integer.parseInt(getCallbackValue(callbackQuery));
        return getPageData(callbackQuery.getFrom().getId(), page, dictionary);
    }

    @Override
    public IMessageData findFilm(Message message, Dictionary dictionary) {
        try {
            CacheKey cacheKey = new CacheKey(message.getFrom().getId(), "");
            List<SimpleFilmInfo> filmList = getDataGeneratorService().getFilmList(message.getText());

            ICacheService<CacheKey, List<SimpleFilmInfo>> cacheService = getCacheService();
            cacheService.put(cacheKey, filmList);

            return getSearchResult(filmList, 0, dictionary);

        } catch (IOException | StorageAccessException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public IMessageData otherFilm(CallbackQuery callbackQuery, Dictionary dictionary) {
        int index = Integer.parseInt(getCallbackValue(callbackQuery));

        return getSearchResult(
                getFilmListFromCache(callbackQuery),
                index,
                dictionary
        );
    }

    private IMessageData getPageData(Integer userId, Integer page, Dictionary dictionary) {
        Page<Film> userFilmListPage = getDAOService().getUserFilmList(userId, page);

        StringBuilder sb = new StringBuilder();
        userFilmListPage.getData().forEach(x -> {
            sb.append(String.format(dictionary.get("msg.format.film.list"), x.getName(), x.getYear(), x.getTime()));
            sb.append(dictionary.get("film.list.delimiters"));
        });

        return new MessageDataImpl(
                sb.toString(),
                getKeyboardService().getKeyboard(Inline.FILMLIST, dictionary, userFilmListPage)
        );
    }

    private MessageDataImpl addFilm(String text, Integer userId, Dictionary dictionary) {
        String txt;

        try {
            Film film = getDataGeneratorService().getFilm(text);
            film.setUserId(userId);
            getDAOService().add(film);

            txt = dictionary.get("msg.film.add");

        } catch (IOException | StorageAccessException e) {
            e.printStackTrace();
            txt = dictionary.get("msg.invalid.link");
        }

        return new MessageDataImpl(txt, null);
    }

    private String getCallbackValue(CallbackQuery callbackQuery) {
        return StringUtils.substringAfterLast(callbackQuery.getData(), DictionaryShelf.get("callback.data.delimiter"));
    }

    private MessageDataImpl getSearchResult(List<SimpleFilmInfo> filmList, int index, Dictionary dictionary) {
        SimpleFilmInfo film = filmList.get(index);

        String txt = String.format(
                dictionary.get("msg.format.search.film"),
                film.getQueryStr(),
                film.getName(),
                film.getOriginalName(),
                film.getYear(),
                film.getTime()
        );

        Pair<Integer, List<SimpleFilmInfo>> keyBoardData = new Pair<>(index, filmList);
        ReplyKeyboard keyboard = getKeyboardService().getKeyboard(Inline.SEARCHFILM, dictionary, keyBoardData);

        return new MessageDataImpl(txt, keyboard);
    }

    private List<SimpleFilmInfo> getFilmListFromCache(CallbackQuery callbackQuery) {
        CacheKey cacheKey = new CacheKey(callbackQuery.getFrom().getId(), "");
        ICacheService<CacheKey, List<SimpleFilmInfo>> cacheService = getCacheService();

        return cacheService.get(cacheKey);
    }
}
