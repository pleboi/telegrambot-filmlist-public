package net.resiststan.filmlist.service.method;

import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboard;

import java.util.Optional;

public class MessageDataImpl implements IMessageData {
    private final String text;
    private final TextPosition textPosition;
    private final Optional<ReplyKeyboard> keyboard;

    public MessageDataImpl(String text, TextPosition textPosition, ReplyKeyboard keyboard) {
        this.text = text;
        this.textPosition = textPosition;
        this.keyboard = Optional.ofNullable(keyboard);
    }

    public MessageDataImpl(String text, ReplyKeyboard keyboard) {
        this.text = text;
        this.textPosition = TextPosition.REPLACE;
        this.keyboard = Optional.ofNullable(keyboard);
    }

    @Override
    public String getText() {
        return text;
    }

    @Override
    public TextPosition getTextPosition() {
        return textPosition;
    }

    @Override
    public Optional<ReplyKeyboard> getKeyboard() {
        return keyboard;
    }
}
