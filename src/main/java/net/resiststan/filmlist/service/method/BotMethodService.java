package net.resiststan.filmlist.service.method;

import org.jetbrains.annotations.NotNull;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;

public class BotMethodService implements IBotMethodService {

    @Override
    public SendMessage createNewMessage(Message message, @NotNull IMessageData messageData) {
        SendMessage sm = new SendMessage();
        sm.setChatId(message.getChatId());

        sm.setText(messageData.getText());
        messageData.getKeyboard().ifPresent(sm::setReplyMarkup);

        return sm;
    }

    @Override
    public EditMessageText createEditMessageText(Message message, @NotNull IMessageData messageData) {
        EditMessageText emt = new EditMessageText();
        emt.setChatId(message.getChatId());
        emt.setMessageId(message.getMessageId());

        switch (messageData.getTextPosition()) {
            case APPEND:
                emt.setText(message.getText() + messageData.getText());
                break;
            case PREPEND:
                emt.setText(messageData.getText() + message.getText());
                break;
            case REPLACE:
            default:
                emt.setText(messageData.getText());
        }

        messageData.getKeyboard().ifPresent(x -> emt.setReplyMarkup((InlineKeyboardMarkup) x));

        return emt;
    }


}
