package net.resiststan.filmlist.service.method;

import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboard;

import java.util.Optional;

public interface IMessageData {
    String getText();

    TextPosition getTextPosition();

    Optional<ReplyKeyboard> getKeyboard();

    enum TextPosition {
        PREPEND,
        REPLACE,
        APPEND
    }
}
