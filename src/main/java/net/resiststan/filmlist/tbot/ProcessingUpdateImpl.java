package net.resiststan.filmlist.tbot;

import net.resiststan.customtelegrambot.util.IUpdateHandler;
import net.resiststan.filmlist.tbot.execute.AbsAnswerFactory;
import net.resiststan.filmlist.tbot.execute.CallbackFactoryImp;
import net.resiststan.filmlist.tbot.execute.MessageFactoryImp;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.io.Serializable;

public class ProcessingUpdateImpl implements IUpdateHandler {
    private final AbsAnswerFactory messageFactory;
    private final AbsAnswerFactory callbackFactory;

    ProcessingUpdateImpl() {
        messageFactory = new MessageFactoryImp();
        callbackFactory = new CallbackFactoryImp();
    }

    @Override
    public BotApiMethod<? extends Serializable> handle(Update update) {
        BotApiMethod<? extends Serializable> method = null;

        if (update.hasMessage() && update.getMessage().hasText()) {
            method = messageFactory.createBy(update);
        } else if (update.hasCallbackQuery()) {
            method = callbackFactory.createBy(update);
        }

        return method;
    }
}
