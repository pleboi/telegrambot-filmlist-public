package net.resiststan.filmlist.tbot;

import net.resiststan.customtelegrambot.CustomBot;

public class Run {
    public static void main(String[] args) {
        CustomBot bot = new CustomBot(new ProcessingUpdateImpl());
        bot.start();
    }
}
