package net.resiststan.filmlist.tbot.execute;

import net.resiststan.filmlist.service.method.IMessageData;
import net.resiststan.filmlist.util.DictionaryShelf;
import org.jetbrains.annotations.Nullable;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.util.Objects;


public class CallbackFactoryImp extends AbsAnswerFactory {

    @Override
    @Nullable
    protected EditMessageText handle(Update update) {
        IMessageData messageData = getMessageData(update.getCallbackQuery());

        if (Objects.nonNull(messageData)) {
            return getBotMethodService().createEditMessageText(
                    update.getCallbackQuery().getMessage(),
                    messageData
            );
        }

        return null;
    }

    @Nullable
    private IMessageData getMessageData(CallbackQuery callbackQuery) {
        String data = callbackQuery.getData();
        DictionaryShelf.Dictionary dictionary = getDictionary(callbackQuery.getFrom().getLanguageCode());

        if (data.equals(getCallbackKey("cb.another.random.film"))) {
            return getMessageDataService().getRandomFilm(callbackQuery.getFrom().getId(), dictionary);

        } else if (data.indexOf(getCallbackKey("cb.watched.film")) == 0) {
            return getMessageDataService().markAsWatchFilm(callbackQuery, dictionary);

        } else if (data.indexOf(getCallbackKey("cb.remove.film")) == 0) {
            return getMessageDataService().removeFilm(callbackQuery, dictionary);

        } else if (data.indexOf(getCallbackKey("ca.add.current.film")) == 0) {
            return getMessageDataService().addFilm(callbackQuery, dictionary);

        } else if (data.indexOf(getCallbackKey("ca.other.film")) == 0) {
            return getMessageDataService().otherFilm(callbackQuery, dictionary);

        } else if (data.indexOf(getCallbackKey("cb.go.to.page")) == 0) {
            return getMessageDataService().getUserPageFilm(callbackQuery, dictionary);

        }

        return null;
    }

    private String getCallbackKey(String key) {
        return DictionaryShelf.get(key);
    }
}
