package net.resiststan.filmlist.tbot.execute;

import net.resiststan.filmlist.service.cache.CacheService;
import net.resiststan.filmlist.service.cache.ICacheService;
import net.resiststan.filmlist.service.dao.DAOService;
import net.resiststan.filmlist.service.dao.IDAOService;
import net.resiststan.filmlist.service.datagenerator.DataGeneratorService;
import net.resiststan.filmlist.service.datagenerator.IDataGeneratorService;
import net.resiststan.filmlist.service.keyboard.IKeyboardService;
import net.resiststan.filmlist.service.keyboard.KeyboardService;
import net.resiststan.filmlist.service.messagedata.IMessageDataService;
import net.resiststan.filmlist.service.messagedata.MessageDataService;
import net.resiststan.filmlist.service.method.BotMethodService;
import net.resiststan.filmlist.service.method.IBotMethodService;
import net.resiststan.filmlist.util.DictionaryShelf;
import org.jetbrains.annotations.Nullable;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.io.Serializable;

import static net.resiststan.filmlist.service.AbsServiceKeeper.getService;

public abstract class AbsAnswerFactory {
    public final BotApiMethod<? extends Serializable> createBy(Update update) {
        return handle(update);
    }

    @Nullable
    protected abstract BotApiMethod<? extends Serializable> handle(Update update);

    public static DictionaryShelf.Dictionary getDictionary(String dictionaryCode) {
        return DictionaryShelf.getDictionary(dictionaryCode);
    }

    public static IDAOService getDAOService() {
        return getService(DAOService.class);
    }

    public static IDataGeneratorService getDataGeneratorService() {
        return getService(DataGeneratorService.class);
    }

    public static IBotMethodService getBotMethodService() {
        return getService(BotMethodService.class);
    }

    public static IKeyboardService getKeyboardService() {
        return getService(KeyboardService.class);
    }

    public static IMessageDataService getMessageDataService() {
        return getService(MessageDataService.class);
    }

    public static ICacheService getCacheService() {
        return getService(CacheService.class);
    }
}
